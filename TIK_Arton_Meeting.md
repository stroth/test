1. [TIK Arton Meeting](#tik-arton-meeting)
   1. [High I/O load on net\_scratch (itetnas04)](#high-io-load-on-net_scratch-itetnas04)
      1. [What is I/O?](#what-is-io)
      2. [What is net\_scratch?](#what-is-net_scratch)
      3. [Interpretation of keywords](#interpretation-of-keywords)
      4. [Primary symptoms of high I/O load](#primary-symptoms-of-high-io-load)
      5. [Secondary symptoms](#secondary-symptoms)
   2. [Kernel bug](#kernel-bug)
   3. [Remedies](#remedies)
      1. [for primary symptoms](#for-primary-symptoms)
      2. [for secondary symptoms](#for-secondary-symptoms)
   4. [Conclusion](#conclusion)
   5. [Further explanations](#further-explanations)
      1. [Untracable I/O through NFS back to a user](#untracable-io-through-nfs-back-to-a-user)
      2. [GPU power usage as metrics](#gpu-power-usage-as-metrics)
      3. [wandb](#wandb)
   6. [Unsortierte Notizen](#unsortierte-notizen)
      1. [Communication / tikgpu01](#communication--tikgpu01)

# TIK Arton Meeting

## High I/O load on net_scratch (itetnas04)

This is a measurable metric and was established SOME_TIME_LAST_YEAR as the bottleneck causing the symptoms listed below.

### What is I/O?

* Streams of data in parallel
* Concurrent read/write streams
* Long-lived streams
* Short-lived streams: Quickly opening and closing small files
* Accessing metadata of files

### What is net_scratch?

* [net_scratch description and terms of use](https://computing.ee.ethz.ch/Services/NetScratch)
* Important keywords in the link above are: **NFS**, **RAID**, **scratch**

### Interpretation of keywords

1. I/O cannot be traced back to the initiating user process through **NFS**, only to the source host as a whole (not out of the box in the current setup like with `nfswatch` and NFSv3, and not in the currect used OS versions, further discussion below)
2. The **RAID** is not built for HPC demands like concurrent parallel read/write access to many small files and its metadata
3. A **scratch** storage's features are not meant to be equivalent to a designated HPC storage

### Primary symptoms of high I/O load

* Slow startup of `conda` environments
* Slow login to ITET workstations when `conda` is initialized in `.bashrc`
* Cancelled jobs did not terminate in time, staying indefinitely in *CG/COMPLETING* state
  
### Secondary symptoms

* Increase of nodes falling into drain states with reason *Kill Task Failed*. This was a result of reboots of `net_scratch` as an attempt to remedy the primary symptoms.

## Kernel bug

* A kernel update with important security fixes was applied to `net_scratch` on 2024-07-16. This update contained a bug which aggravated the problem from delays up to timeouts.
* As a temporary workaround the kernel was rolled back on Wed Jul 31 05:39:09 2024.
* [Kernel bug](https://lore.kernel.org/linux-nfs/76a841e5-4e3d-4942-9a4b-24f87d6b79a5@molgen.mpg.de/) so far only triggers under specific load circumstances of `net_scratch` and does not show up on other NFS servers with same kernel in ISG environment. This makes it harder to debug.
* Upstream maintainers asked to bisect changes but without being able to reproduce the issue on a non-production system this becomes hard task
* Needs a solution as it is not an option to stick indefintively on the old kernel.

## Remedies

### for primary symptoms

Consolidation of the already available *HPC storage best practices* in the Computing wiki on one Wiki page:

* No conda initialization in `.bashrc`
* How to use local scratch of nodes through `scratch_net`
* Using `micromamba` environments
* Where to place temporary files
* Automatic cleanup of temporary files

Additions to such a *HPC storage best practices*:

* Using environments in compressed squashfs images by means of `squashfs-mount`
* `wandb` best practice
* Don't move data to RAM. Linux already caches file access in RAM, but only if it's available.

### for secondary symptoms

* Automated reboot when nodes with such states are drained will be put into effect wit a grace time of 2 hours for us to react and intervene if necessary.

## Conclusion

* If you want to use `net_scratch`, stick to best practices
* Otherwise we'll gladly host a storage system for TIK (Keep in mind that accessing a lot of smal files is bad performance-wise for almost any built system)

## Further explanations

### Untracable I/O through NFS back to a user

* `nfsd` is used to communicate between client (node) and server (net_scratch)
* nfs connections of a job can't currently be controlled and limited by Slurm. Neither can I/O per job with cgroups
* `nfsd` on the server side does not show I/O for individual users/processes (`nfsiostat` aggregates per mount point)
* `iotop` can show I/O per process on a system. But `nfsd` processes are run as `root`
* On the server it's possible to see the node where I/O is initiated from
  - Sysadmins have to get a view by collecting/correlating various data sources, like network traffic, Slurmjobs, host specific metrics (nfsd, kernel, ...)
  - traffic inspection for long term checking packets content (uid, client, ...) will cause drop on performance on such high load server likely
  - no option to enable nfsd/rpc debugging on such high load system as this immediately will cause system performance to degrade or system stuck
  - tools might exists in future like nfstop or bpf based solutions wich I'm researching for a while already (as replacement for `nfswatch`)
  - With recent kernels as used on the system we get some more information on clients trough `/proc/fs/nfsd` on open files from clients
  - Later upgrade to at least Debian bookworm will enable us to use tools using these ifnormation to make it more readable (`nfsdclnts` since `nfs-utils` 2.4.4, but standalone usable with 5.10.y kernel already using the `/proc` interface)
* In Slurm it's possible to see which users run jobs on a node

That's it. It's not possible to tell which job is causing I/O.

### GPU power usage as metrics

This correlates nicely with GPU-Core utilization according to our experience. But this is not the issue here.

### wandb

Use of `wandb` is new, only [3 years old](https://wandb.ai/wandb_fc/articles/reports/Why-I-Started-Weights-Biases--Vmlldzo1NDcxMTE2).
From our point of view it's use correllates with rising I/O load on `net_scratch`.

## Unsortierte Notizen

### Communication / tikgpu01

* TIK's IT support contact is <servicedesk-tik@id.ethz.ch> (see <https://www.s4d.id.ethz.ch/d-itet/>). We communicate changes to this address, they communicate it to the TIK group.
* For changes to the TIK Slurm configuration you have designated technical contacts, Benjamin and Florian, which act as intermediaries between your group and us. In this particular case communication seems to have not went well, even though Edo and Salvatore were in contact Salvatore bleives some mistakes have happened that the information did not went down to the users.
* And of course you can send us tickets.
* The infrastructure we provide is documented in our Computing wiki. Links to this wiki are provided in presentation to new students in a shortened version of our [Infoposter](https://infoposter.ee.ethz.ch/) as well as the README.txt in every new user home of a D-ITET account. We always appreciate feedback on how to improve this wiki.
* We cannot force people to read and apply the information we provide. If there's a technical solution we try to implement it.
* Some decisions for `net_scratch` were deliberately choosen, e.g. to not impose quotas on users (other HPC with targeted storage for computing solely in the cluster do that and e.g. impose quotas on amount of files, and users are naturaly forced to live with those restrictions and follow guidelines)
* Deadlines: We don't know your deadlines. You can tell us and ask for increased support presence when you approach deadlines. CVL does this and pays for it as well.
* There is no ISG Slurm tool, that was a misunderstanding.
